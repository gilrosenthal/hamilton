/* global $ */
function fetchJSONFile(path, callback) {
    var httpRequest = new XMLHttpRequest();
    httpRequest.onreadystatechange = function() {
        if (httpRequest.readyState === 4) {
            if (httpRequest.status === 200) {
                var data = JSON.parse(httpRequest.responseText);
                if (callback) callback(data);
            }
        }
    };
    httpRequest.open('GET', path);
    httpRequest.send();
}

function goToProp() {
    window.href = ("/property/" + document.getElementById("search").value);
}

function getCookie(name) {
    var value = "; " + document.cookie;
    var parts = value.split("; " + name + "=");
    if (parts.length == 2) return parts.pop().split(";").shift();
}

var propname = getCookie("propname");
var dict = {};
fetchJSONFile("/editdictionary.json", function(data) {
    dict = data;
});
fetchJSONFile("/api/" + propname, function(data) {
    setProperty(data);
    setAC(data);
    setDoors(data);
    setElectrical(data);
    setElevator(data);
    setExtermination(data);
    setFire(data);
    setGas(data);
    setHeating(data);
    setLaundry(data);
    setLock(data);
    setPersonnel(data);
    setPlumbing(data);
    setRoof(data);
    setSnow(data);
    setSprinkler(data);
    setTowing(data);
    setTrash(data);
    setWater(data);
});

function setProperty(data) {
    for (var type in data) {
        if (data[type].value.toString() !== "[object Object]") {
            $(dict[type]).val(data[type].value);
        }
    }
}

function setAC(data) {
    if (data.AC) {
        for (var type in data.AC.value) {
            if (data.AC.value[type].toString() !== "[object Object]") {
                $(dict.AC[type]).val(data.AC.value[type]);
            }
        }
    }
}

function setDoors(data) {
    if (data["Doors and Windows"]) {
        for (var type in data["Doors and Windows"].value) {
            if (data["Doors and Windows"].value[type].toString() !== "[object Object]") {
                $(dict["Doors and Windows"][type]).val(data["Doors and Windows"].value[type]);
            }
        }
    }
}

function setElectrical(data) {
    if (data.Electrical) {
        for (var type in data.Electrical.value) {
            if (data.Electrical.value[type].toString() !== "[object Object]") {
                $(dict.Electrical[type]).val(data.Electrical.value[type]);
            }
        }
    }
}

function setElevator(data) {
    if (data.Elevator) {
        for (var type in data.Elevator.value) {
            if (data.Elevator.value[type].toString() !== "[object Object]") {
                $(dict.Elevator[type]).val(data.Elevator.value[type]);
            }
        }
    }
}

function setExtermination(data) {
    if (data.Extermination) {
        for (var type in data.Extermination.value) {
            if (data.Extermination.value[type].toString() !== "[object Object]") {
                $(dict.Extermination[type]).val(data.Extermination.value[type]);
            }
        }
    }
}

function setFire(data) {
    if (data.Fire) {
        for (var type in data["Fire Alarm"].value) {
            if (data["Fire Alarm"].value[type].toString() !== "[object Object]") {
                $(dict["Fire Alarm"][type]).val(data["Fire Alarm"].value[type]);
            }
        }
    }
}

function setGas(data) {
    if (data.Gas) {
        for (var type in data.Gas.value) {
            if (data.Gas.value[type].toString() !== "[object Object]") {
                $(dict.Gas[type]).val(data.Gas.value[type]);
            }
        }
    }
}

function setHeating(data) {
    if (data.Heating) {
        for (var type in data.Heating.value) {
            if (data.Heating.value[type].toString() !== "[object Object]") {
                $(dict.Heating[type]).val(data.Heating.value[type]);
            }
        }
    }
}

function setLaundry(data) {
    if (data.Laundry) {
        for (var type in data.Laundry.value) {
            if (data.Laundry.value[type].toString() !== "[object Object]") {
                $(dict.Laundry[type]).val(data.Laundry.value[type]);
            }
        }
    }
}

function setLock(data) {
    if (data.Locksmith) {
        for (var type in data.Locksmith.value) {
            if (data.Locksmith.value[type].toString() !== "[object Object]") {
                $(dict.Locksmith[type]).val(data.Locksmith.value[type]);
            }
        }
    }
}

function setPersonnel(data) {
    if (data["Personnel and Cleaning"]) {
        for (var type in data["Personnel and Cleaning"].value) {
            if (data["Personnel and Cleaning"].value[type].toString() !== "[object Object]") {
                $(dict["Personnel and Cleaning"][type]).val(data["Personnel and Cleaning"].value[type]);
            }
        }
    }
}

function setPlumbing(data) {
    if (data.Plumbing) {
        for (var type in data.Plumbing.value) {
            if (data.Plumbing.value[type].toString() !== "[object Object]") {
                $(dict.Plumbing[type]).val(data.Plumbing.value[type]);
            }
        }
    }
}

function setRoof(data) {
    if (data.Roof) {
        for (var type in data.Roof.value) {
            if (data.Roof.value[type].toString() !== "[object Object]") {
                $(dict.Roof[type]).val(data.Roof.value[type]);
            }
        }
    }
}

function setSnow(data) {
    if (data.Snow) {
        for (var type in data["Snow Services"].value) {
            if (data["Snow Services"].value[type].toString() !== "[object Object]") {
                $(dict["Snow Services"][type]).val(data["Snow Services"].value[type]);
            }
        }
    }
}

function setSprinkler(data) {
    if (data.Sprinkler) {
        for (var type in data["Sprinkler System"].value) {
            if (data["Sprinkler System"].value[type].toString() !== "[object Object]") {
                $(dict["Sprinkler System"][type]).val(data["Sprinkler System"].value[type]);
            }
        }
    }
}

function setTowing(data) {

    if (data.Towing) {
        for (var type in data.Towing.value) {
            if (data.Towing.value[type].toString() !== "[object Object]") {
                $(dict.Towing[type]).val(data.Towing.value[type]);
            }
        }
    }
}

function setTrash(data) {
    if (data.Trash) {
        for (var type in data.Trash.value) {
            if (data.Trash.value[type].toString() !== "[object Object]") {
                $(dict.Trash[type]).val(data.Trash.value[type]);
            }
        }
    }
}

function setWater(data) {
    if (data["Water Meter"]) {
        for (var type in data["Water Meter"].value) {
            if (data["Water Meter"].value[type].toString() !== "[object Object]") {
                $(dict["Water Meter"][type]).val(data["Water Meter"].value[type]);
            }
        }
    }
}
$(function() {
    $('#toggleprop').click(function() {
        $('#propertyInfo').toggle();
    });
    $('#toggleac').click(function() {
        $('#acinfo').toggle();
    });
    $('#toggledoors').click(function() {
        $('#doorsinfo').toggle();
    });
    $('#toggleelectrical').click(function() {
        $('#electricalinfo').toggle();
    });
    $('#toggleelevator').click(function() {
        $('#elevatorinfo').toggle();
    });
    $('#toggleextermination').click(function() {
        $('#exterminationinfo').toggle();
    });
    $('#togglefirealarm').click(function() {
        $('#firealarminfo').toggle();
    });
    $('#togglegas').click(function() {
        $('#gasinfo').toggle();
    });
    $('#toggleheating').click(function() {
        $('#heatinginfo').toggle();
    });
    $('#togglelaundry').click(function() {
        $('#laundryinfo').toggle();
    });
    $('#togglelocksmith').click(function() {
        $('#locksmithinfo').toggle();
    });
    $('#togglepersonnel').click(function() {
        $('#personnelinfo').toggle();
    });
    $('#toggleplumbing').click(function() {
        $('#plumbinginfo').toggle();
    });
    $('#toggleroof').click(function() {
        $('#roofinfo').toggle();
    });
    $('#togglesnow').click(function() {
        $('#snowinfo').toggle();
    });
    $('#togglesprinkler').click(function() {
        $('#sprinklerinfo').toggle();
    });
    $('#toggletrash').click(function() {
        $('#trashinfo').toggle();
    });
    $('#togglewater').click(function() {
        $('#waterinfo').toggle();
    });
    $("#submit").click(function() {
        var data = {};
        data.propertyid = $("#property").val();
        data.prop = {
            "Address": $("#Address").val(),
            "PropertyName": $("#PropertyName").val(),
            "PropertyType": $("#PropertyType").val(),
            "PropertyMgr": $("#PropertyMgr").val(),
            "PropertyCoordinator": $("#PropertyCoordinator").val(),
            "numFloors": $("#numFloors").val(),
            "numUnits": $("#numUnits").val(),
            "numBasementUnits": $("#numBasementUnits").val(),
            "UnitsPerFloor": $("#UnitsPerFloor").val(),
            "ConstructionType": $("#ConstructionType").val(),
            "BuildingType": $("#BuildingType").val(),
            "6StoryApartment": $("#6StoryApartment").val(),
            "ExteriorCovering": $("#ExteriorCovering").val(),
            "BuildingAge": $("#BuildingAge").val(),
            "PropertySize": $("#PropertySize").val(),
            "numofBuildings": $("#numOfBuildings").val(),
            "CurrentUse": $("#CurrentUse").val(),
            "Comments": $("#Comments").val(),
        };
        data.ac = {
            "Company": $("#ACCompany").val(),
            "Phone": $("#ACCompanyPhone").val(),
            "Contract": $("#ACContract").val(),
            "TypeofHVACSystem": $("#TypeofHVACSystem").val(),
            "Central": $("#Central").val(),
            "WallUnit": $("#WallUnit").val(),
            "Location": $("#ACLocation").val(),
            "CoolingTower": $("#CoolingTower").val(),
            "Maintenance": $("#Maintenance").val(),
            "ChemicalTreatment": $("#ChemicalTreatment").val(),
            "FiltersChangeddate": $("#FiltersChangeddate").val(),
        };
        data.doors = {
            "BuildingFrontDoorType": $("#BuildingFrontDoorType").val(),
            "FrontDoorKeyType": $("#FrontDoorKeyType").val(),
            "FrontDoorCode": $("#FrontDoorCode").val(),
            "FrontSecurityDoorType": $("#FrontSecurityDoorType").val(),
            "FrontSecurityDoorKeyType": $("#FrontSecurityDoorKeyType").val(),
            "FrontSecurityDoorCode": $("#FrontSecurityDoorCode").val(),
            "RearSecurityDoortype": $("#RearSecurityDoortype").val(),
            "RearSecurityDoorKeyType": $("#RearSecurityDoorKeyType").val(),
            "RearSecurityDoorCode": $("#RearSecurityDoorCode").val(),
            "GarageDoor": $("#GarageDoor").val(),
            "Company": $("#DoorsCompany").val(),
            "CompanyPhone": $("#DoorsCompanyPhone").val(),
            "Windows": $("#Windows").val(),
            "Contractor": $("#DoorsContractor").val(),
            "ContractorPhone": $("#DoorsContractorPhone").val(),
            "StormWindows": $("#StormWindows").val(),
            "Other": $("#DoorsOther").val(),
        };
        data.electrical = {
            "ServiceCompany": $("#ServiceCompany").val(),
            "Phone": $("#ServiceCompanyPhone").val(),
            "AptCircuitBreaker": $("#AptCircuitBreaker").val(),
            "FuseBox": $("#FuseBox").val(),
            "LocationofService": $("#LocationofService").val(),
            "InApartment": $("#InApartment").val(),
            "Amps": $("#Amps").val(),
            "PublicCircuitBreaker": $("#PublicCircuitBreaker").val(),
            "PublicFuseBox": $("#PublicFuseBox").val(),
            "LightTimer": $("#LightTimer").val(),
            "PhotoCells": $("#PhotoCells").val(),
            "Location": $("#ElectricalLocation").val()
        };
        data.elevator = {
            "Contractor": $("#ElevatorContractor").val(),
            "ContractorPhone": $("#ElevatorContractorPhone").val(),
            "Passenger": $("#Passenger").val(),
            "numOfElevators": $("#numOfElevators").val(),
            "Freight": $("#Freight").val(),
            "ElevatorRoomLocation": $("#ElevatorRoomLocation").val(),
            "KeyType": $("#ElevatorKeyType").val(),
            "CommentsRepair": $("#CommentsRepair").val(),
            "InspectionDate": $("#InspectionDate").val(),
            "customerNumber": $("#customerNumber").val(),
            "ContactAndPhone": $("#ContactAndPhone").val(),
        };
        data.extermination = {
            "Company": $("#ExterminationCompany").val(),
            "CompanyPhone": $("#ExterminationCompanyPhone").val(),
            "Contact": $("#ExterminationContact").val(),
            "ContactPhone": $("#ExterminationContactPhone").val(),
            "Contract": $("#ExterminationContract").val(),
            "Frequency": $("#ExterminationFrequency").val()
        };
        data.firealarm = {
            "MonitorCompany": $("#MonitorCompany").val(),
            "MonitorCompanyPhone": $("#MonitorCompanyPhone").val(),
            "accountnumber": $("#accountnumber").val(),
            "TenantAlarm": $("#TenantAlarm").val(),
            "TenantAlarmPhone": $("#TenantAlarmPhone").val(),
            "EmergencyServiceCompany": $("#EmergencyServiceCompany").val(),
            "EmergencyServiceCompanyPhone": $("#EmergencyServiceCompanyPhone").val(),
            "Contract": $("#FireAlarmContract").val(),
            "Location": $("#FireAlarmLocation").val(),
            "Passcode": $("#FireAlarmPasscode").val(),
            "Central_Station": $("#Central_Station").val(),
            "PhonelinetoFireDept": $("#PhonelinetoFireDept").val(),
            "SmokeDetectors": $("#SmokeDetectors").val(),
            "HardWire": $("#HardWire").val(),
            "Battery": $("#Battery").val(),
            "HeatDetectors": $("#HeatDetectors").val(),
            "Located": $("#Located").val(),
            "AdditonalEquipment": $("#AdditonalEquipment").val(),
            "InspectionDate": $("#FireInspectionDate").val(),
            "KeyType": $("#KeyType").val(),
            "Code": $("#FireCode").val()
        };
        data.gas = {
            "Company": $("#GasCompany").val(),
            "SuppliedByLandlord": $("#SuppliedByLandlord").val(),
            "MeterLocation": $("#MeterLocation").val(),
            "Boilers": $("#Boilers").val(),
            "Stoves": $("#Stoves").val(),
            "EmergencyService": $("#GasEmergencyService").val(),
            "EmergencyPhone": $("#GasEmergencyPhone").val()
        };
        data.heating = {
            "type": $("#Type").val(),
            "location": $("#HeatSourceLocation").val(),
            "heatingCompany": $("#heatingCompany").val(),
            "phone": $("#HeatingCompanyPhone").val(),
            "other": $("#HeatingOther").val(),
            "contract": $("#HeatingContract").val(),
            "fhw": $("#fhw").val(),
            "steam": $("#steam").val(),
            "fha": $("#fha").val(),
            "electric": $("#electric").val(),
            "hvac": $("#hvac").val(),
            "basementappt": $("#basementappt").val(),
            "laundryroom": $("#laundryroom").val(),
            "laundrynotes": $("#laundrynotes").val(),
            "heatpaidforby": $("#heatpaidforby").val(),
            "electricpaidforby": $("#electricpaidForby").val(),
            "thcowned": $("#heatthcowned").val(),
            "individual": $("#individual").val(),
            "oilfilllocationsandtanksize": $("#oilfilllocationsandtanksize").val(),
            "hotwatersuppliedby": $("#hotwatersuppliedby").val(),
            "heattimerlocation": $("#heattimerlocation").val(),
            "boilercleanoutdate": $("#boilercleanoutdate").val(),
            "comments": $("#heatcomments").val()
        };
        data.laundry = {
            "Location": $("#LaundryLocation").val(),
            "ServiceCompany": $("#LaundryServiceCompany").val(),
            "ServiceCompanyPhone": $("#LaundryServiceCompanyPhone").val(),
            "THCowned": $("#THCowned").val(),
            "Leased": $("#Leased").val(),
            "NumberofWashers": $("#NumberofWashers").val(),
            "NumberofDryers": $("#NumberofDryers").val(),
            "CostofWashers": $("#CostofWashers").val(),
            "CostofDryers": $("#CostofDryers").val(),
            "MeterBoxCheckDate": $("#MeterBoxCheckDate").val(),
            "CardCoin": $("#CardCoin").val(),
            "VTMLocation": $("#VTMLocation").val(),
            "VTMLine": $("#VTMLine").val(),
            "CardCompany": $("#CardCompany").val()
        };
        data.locksmith = {
            "EmergencyLockSmith": $("#EmergencyLockSmith").val(),
            "EmergencyLockPhone": $("#EmergencyLockPhone").val(),
            "LocationOfKeys": $("#LocationOfKeys").val(),
            "FrontRearDoorMastered": $("#FrontRearDoorMastered").val(),
            "UnitsMastered": $("#UnitsMastered").val(),
            "BoilerRoomLockAndLocation": $("#BoilerRoomLockAndLocation").val(),
            "FobRepairContact": $("#FobRepairContact").val(),
            "FobOrdering": $("#FobOrdering").val()
        };
        data.personnel = {
            "24hroncallname": $("#24hroncallname").val(),
            "24hroncallphone": $("#24hroncallphone").val(),
            "24hroncallemail": $("#24hroncallemail").val(),
            "supername": $("#supername").val(),
            "superphone": $("#superphone").val(),
            "superemail": $("#superemail").val(),
            "superaddress": $("#superaddress").val(),
            "onsitemanagername": $("#onsitemanagername").val(),
            "onsitemanagerphone": $("#onsitemanagerphone").val(),
            "onsitemanageraddress": $("#onsitemanageraddress").val(),
            "securitydesk": $("#securitydesk").val(),
            "securitydeskphone": $("#securitydeskphone").val(),
            "afterhoursaccess": $("#afterhoursaccess").val(),
            "guardservice": $("#guardservice").val(),
            "guardservicephone": $("#guardservicephone").val(),
            "cleaningcompany": $("#cleaningcompany").val(),
            "cleaningcompanyphone": $("#cleaningcompanyphone").val(),
            "cleaningspecifications": $("#cleaningspecifications").val()
        };
        data.plumbing = {
            "EmergencyService": $("#plumbingEmergencyService").val(),
            "EmergencyPhone": $("#plumbingEmergencyServicePhone").val(),
            "BackUpService": $("#BackUpService").val(),
            "BackUpPhone": $("#BackUpPhone").val(),
            "EmergencySewerAndDrain": $("#EmergencySewerAndDrain").val(),
            "EmergencySewerAndDrainPhone": $("#EmergencySewerAndDrainPhone").val(),
            "FloodsWetVacCompany": $("#FloodsWetVacCompany").val(),
            "FloodsWetVacCompanyPhone": $("#FloodsWetVacCompanyPhone").val()
        };
        data.roof = {
            "Contractor": $("#RoofContractor").val(),
            "ContractorPhone": $("#RoofContractorPhone").val(),
            "RoofAccessLocation": $("#RoofAccessLocation").val(),
            "Type": $("#RoofType").val(),
            "Age": $("#RoofAge").val(),
            "Warranty": $("#RoofWarranty").val()
        };
        data.snow = {
            "PlowingTHCOrPrivate": $("#PlowingTHCOrPrivate").val(),
            "Contact": $("#PlowingContact").val(),
            "ContactPhone": $("#PlowingContactPhone").val(),
            "ShovelingTHCOrPrivate": $("#ShovelingTHCOrPrivate").val(),
            "ShovelingPhone": $("#ShovelingPhone").val(),
            "ShovelingContact": $("#ShovelingContact").val(),
            "ShovelingContactPhone": $("#ShovelingContactPhone").val(),
            "RemovalCompany": $("#RemovalCompany").val(),
            "RemovalPhone": $("#RemovalPhone").val(),
            "MinimumMatToPlow": $("#MinimumMatToPlow").val(),
            "MinimumToShovel": $("#MinimumToShovel").val()
        };
        data.sprinkler = {
            "Company": $("#SprinklerCompany").val(),
            "Phone": $("#SprinklerCompanyPhone").val(),
            "Contract": $("#SprinklerContract").val(),
            "EmergencyPhone": $("#SprinklerEmergencyPhone").val(),
            "ContactPerson": $("#ContactPerson").val(),
            "CommonHallways": $("#CommonHallways").val(),
            "InsideUnits": $("#InsideUnits").val(),
            "WetOrDry": $("#WetOrDry").val(),
            "InspectionDate": $("#SprinklerInspectionDate").val(),
            "LocationOfShutOff": $("#LocationOfShutOff").val()
        };
        data.trash = {
            "Dumpsters": $("#Dumpsters").val(),
            "OwnedOrLeased": $("#OwnedOrLeased").val(),
            "Type": $("#DumpsterType").val(),
            "Location": $("#DumpsterLocation").val(),
            "PickUpTrashDays": $("#PickUpTrashDays").val(),
            "CityPickUp": $("#CityPickUp").val(),
            "CityTrashPhone": $("#CityTrashPhone").val(),
            "PrivatePickUp": $("#PrivatePickUp").val(),
            "Other": $("#TrashOther").val()
        };
        data.water = {
            "TotalNumOfMeters": $("#TotalNumOfMeters").val(),
            "KeyType": $("#WaterKeyType").val(),
            "Code": $("#WaterCode").val(),
            "MainShutOffLocation": $("#MainShutOffLocation").val(),
            "MeterReadDate": $("#MeterReadDate").val()
        };
        data.towing = {
            "TowingCompany": $("#TowingCompany").val(),
            "TowingPhone": $("#TowingPhone").val()
        };
        $.post("/property/" + propname + "/edit", data, function() {}, "json");
    });
});