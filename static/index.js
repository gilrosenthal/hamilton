function fetchJSONFile(path, callback) {
    var httpRequest = new XMLHttpRequest();
    httpRequest.onreadystatechange = function() {
        if (httpRequest.readyState === 4) {
            if (httpRequest.status === 200) {
                var data = JSON.parse(httpRequest.responseText);
                if (callback) callback(data);
            }
        }
    };
    httpRequest.open('GET', path);
    httpRequest.send();
}
var format = "n";
var order = "a";
var fulldata = [];
var originaldata = [];
fetchJSONFile("/api/list", function(data) {
    for (var prop in data) {
        data[prop].Address = parseAddress.parseLocation(data[prop].Address);
    }
    for (var d in data) {
        fulldata[d] = data[d];
        originaldata[d] = data[d];
    }

    displayNormal(fulldata);
});

function displayNormal(scruup) {
    var data = scruup;
    var list = document.getElementById("proplist");
    list.innerHTML = "";
    for (var prop in data) {
        if (!data[prop].Address.number) {
            list.innerHTML += '<li role="presentation"><a href="/property/' + data[prop].property + '">' + data[prop].property + '</a></li>';
        } else {
            if (!data[prop].Address.zip) data[prop].Address.zip = "";
            list.innerHTML += '<li role="presentation"><a href="/property/' + data[prop].property + '">' + data[prop].property + ", " + data[prop].Address.number + " " + data[prop].Address.street + " " + data[prop].Address.type + ", " + data[prop].Address.city + ", " + data[prop].Address.state + ", " + data[prop].Address.zip + '</a></li>';
        }
    }
}

function displayInWorkerFormat(scruup) {
    format = "w";
    order = "a";
    var loop = scruup.slice(0);
    var list = document.getElementById("proplist");
    list.innerHTML = "";
    for (var prop in loop) {
        if (!loop[prop].Address.number) {
            list.innerHTML += '<li role="presentation"><a href="/property/' + loop[prop].property + '">' + loop[prop].property + '</a></li>';
        } else {
            list.innerHTML += '<li role="presentation"><a href="/property/' + loop[prop].property + '">' + loop[prop].Address.street + " " + loop[prop].Address.type + ", " + loop[prop].Address.number + ", " + loop[prop].property + '</a></li>';
        }
    }
}

function makeDescending() {
    var list = document.getElementById("proplist");
    list.innerHTML = "";

    var ne = originaldata.slice(0);
    ne.reverse();
    console.log(ne);
    if (format === "w") {
        displayInWorkerFormat(ne);
    } else {
        displayNormal(ne);
    }

}

function makeAscending() {
    var list = document.getElementById("proplist");
    list.innerHTML = "";

    var ne = originaldata.slice(0);
    if (format === "w") {
        displayInWorkerFormat(ne);
    } else {
        displayNormal(ne);
    }
}
var error = getCookie("error");
if(error){
document.getElementById("propertyinfo").innerHTML = '<div class="alert alert-danger" role="alert">' + error +'</div>';
delete_cookie("error");
}