$(function() {
    $('#propform').submit(function() {
        window.location = "/property/" + document.getElementById("search").value;
        return false;
    });
    if(getCookie("sessionid")) $("#edit").attr("href", window.location +"/edit");
    else{
        $("#edit").hide();
    }
});