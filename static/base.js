function fetchJSONFile(path, callback) {
    var httpRequest = new XMLHttpRequest();
    httpRequest.onreadystatechange = function() {
        if (httpRequest.readyState === 4) {
            if (httpRequest.status === 200) {
                var data = JSON.parse(httpRequest.responseText);
                if (callback) callback(data);
            }
        }
    };
    httpRequest.open('GET', path);
    httpRequest.send();
}

function goToProp() {
    window.href = ("/property/" + document.getElementById("search").value);
}

function getCookie(name) {
    var value = "; " + document.cookie;
    var parts = value.split("; " + name + "=");
    if (parts.length == 2) return parts.pop().split(";").shift();
}
var delete_cookie = function(name) {
    document.cookie = name + '=;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
};
function createCookie(name,value) {
    document.cookie = name + "=" + value;
}
$(function() {
    var uname = getCookie("sessionid");
    if (uname) {
        uname = window.atob(uname);
        $("#uname").html(uname);
        $("#log").html('<a id="logout" href="#">Log out </a>')
    } else {
        $("#uname").html("Guest");
        $("#log").html('<a href="/login">Log in</a>');
        //$("#lo").html('<a href="/signup">Sign up</a> ');
        $("#navbar > li:nth-child(2)").html("");
    }
    $("#logout").click(function() {
        document.cookie.split(";").forEach(function(c) { document.cookie = c.replace(/^ +/, "").replace(/=.*/, "=;expires=" + new Date().toUTCString() + ";path=/"); });
        window.location.href="/";
    });
})