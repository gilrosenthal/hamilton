function fetchJSONFile(path, callback) {
    var httpRequest = new XMLHttpRequest();
    httpRequest.onreadystatechange = function() {
        if (httpRequest.readyState === 4) {
            if (httpRequest.status === 200) {
                var data = JSON.parse(httpRequest.responseText);
                if (callback) callback(data);
            }
        }
    };
    httpRequest.open('GET', path);
    httpRequest.send();
}

function goToProp() {
    window.href = ("/property/" + document.getElementById("search").value);
}

function getCookie(name) {
    var value = "; " + document.cookie;
    var parts = value.split("; " + name + "=");
    if (parts.length == 2) return parts.pop().split(";").shift();
}

var propname = getCookie("propname");
document.getElementById("propform").onsubmit = goToProp;
document.title = propname;
fetchJSONFile("/api/" + propname, function(data) {
    if (data.Alert === "Property not found!") {
        document.getElementById("propertyinfo").innerHTML = '<div class="alert alert-danger" role="alert">Property not found! These are the properties we have in the database:</div>';
        fetchJSONFile("/api/list", function(data) {
            var list = document.getElementById("proplist");
            for (var prop in data) {
                list.innerHTML += '<li role="presentation"><a href="/property/' + data[prop].property + '">' + data[prop].property + '</a></li>';
            }

        });
    } else {
        document.getElementById("prp").innerHTML += propname;
        var i = -1;
        for (var prop in data) {
            i++;
            if (data[prop].value.toString() !== "[object Object]") {
                if (data[prop].value === "") {} else {
                    document.getElementById("propertyinfo").innerHTML += "<div id='" + data[prop].desc + "'>" +
                        "<h2>" + data[prop].desc + "</h2>" +
                        "<p>" + data[prop].value + "</p>" +
                        "</div>";
                }
            } else {
                delete data[prop].value["Property"];
                if (data[prop].value[Object.keys(data[prop].value)[0]] !== "") {
                    var beg = "<div id='" + Object.keys(data)[i] + "'>" +
                        "<h2>" + Object.keys(data)[i] + "</h2>";
                    document.getElementById("listofthings").innerHTML += '<li><a href="#' + Object.keys(data)[i] + '">' + Object.keys(data)[i] + '</a></li>';
                    var html = "";
                    for (var min in data[prop].value) {
                        if (!data[prop].desc[min]) {} else if (data[prop].value[min].toString() === "[object Object]") {} else {
                            html += "<h4>" + data[prop].desc[min] + "</h4><p>" + data[prop].value[min] + "</p>";
                        }
                    }
                    var end = "</div>";
                    document.getElementById("propertyinfo").innerHTML += beg + html + end;
                }
            }
        }
    }
});