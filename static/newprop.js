/*global $ */
$(function() {
    $('#toggleprop').click(function() {
        $('#propertyInfo').toggle();
    });
    $('#toggleac').click(function() {
        $('#acinfo').toggle();
    });
    $('#toggledoors').click(function() {
        $('#doorsinfo').toggle();
    });
    $('#toggleelectrical').click(function() {
        $('#electricalinfo').toggle();
    });
    $('#toggleelevator').click(function() {
        $('#elevatorinfo').toggle();
    });
    $('#toggleextermination').click(function() {
        $('#exterminationinfo').toggle();
    });
    $('#togglefirealarm').click(function() {
        $('#firealarminfo').toggle();
    });
    $('#togglegas').click(function() {
        $('#gasinfo').toggle();
    });
    $('#toggleheating').click(function() {
        $('#heatinginfo').toggle();
    });
    $('#togglelaundry').click(function() {
        $('#laundryinfo').toggle();
    });
    $('#togglelocksmith').click(function() {
        $('#locksmithinfo').toggle();
    });
    $('#togglepersonnel').click(function() {
        $('#personnelinfo').toggle();
    });
    $('#toggleplumbing').click(function() {
        $('#plumbinginfo').toggle();
    });
    $('#toggleroof').click(function() {
        $('#roofinfo').toggle();
    });
    $('#togglesnow').click(function() {
        $('#snowinfo').toggle();
    });
    $('#togglesprinkler').click(function() {
        $('#sprinklerinfo').toggle();
    });
    $('#toggletrash').click(function() {
        $('#trashinfo').toggle();
    });
    $('#togglewater').click(function() {
        $('#waterinfo').toggle();
    });
    $("#property").change(function(){
        
    });
    $("#submit").click(function() {
        var data = {};
        data.propertyid = $("#property").val();
        data.prop = {
            "Address": $("#Address").val(),
            "PropertyName": $("#PropertyName").val(),
            "PropertyType": $("#PropertyType").val(),
            "PropertyMgr": $("#PropertyMgr").val(),
            "PropertyCoordinator": $("#PropertyCoordinator").val(),
            "numFloors": $("#numFloors").val(),
            "numUnits": $("#numUnits").val(),
            "numBasementUnits": $("#numBasementUnits").val(),
            "UnitsPerFloor": $("#UnitsPerFloor").val(),
            "ConstructionType": $("#ConstructionType").val(),
            "BuildingType": $("#BuildingType").val(),
            "6StoryApartment": $("#6StoryApartment").val(),
            "ExteriorCovering": $("#ExteriorCovering").val(),
            "BuildingAge": $("#BuildingAge").val(),
            "PropertySize": $("#PropertySize").val(),
            "numofBuildings": $("#numOfBuildings").val(),
            "CurrentUse": $("#CurrentUse").val(),
            "Comments": $("#Comments").val(),
        };
        data.ac = {
            "Company": $("#ACCompany").val(),
            "Phone": $("#ACCompanyPhone").val(),
            "Contract": $("#ACContract").val(),
            "TypeofHVACSystem": $("#TypeofHVACSystem").val(),
            "Central": $("#Central").val(),
            "WallUnit": $("#WallUnit").val(),
            "Location": $("#ACLocation").val(),
            "CoolingTower": $("#CoolingTower").val(),
            "Maintenance": $("#Maintenance").val(),
            "ChemicalTreatment": $("#ChemicalTreatment").val(),
            "FiltersChangeddate": $("#FiltersChangeddate").val(),
        };
        data.doors = {
            "BuildingFrontDoorType": $("#BuildingFrontDoorType").val(),
            "FrontDoorKeyType": $("#FrontDoorKeyType").val(),
            "FrontDoorCode": $("#FrontDoorCode").val(),
            "FrontSecurityDoorType": $("#FrontSecurityDoorType").val(),
            "FrontSecurityDoorKeyType": $("#FrontSecurityDoorKeyType").val(),
            "FrontSecurityDoorCode": $("#FrontSecurityDoorCode").val(),
            "RearSecurityDoortype": $("#RearSecurityDoortype").val(),
            "RearSecurityDoorKeyType": $("#RearSecurityDoorKeyType").val(),
            "RearSecurityDoorCode": $("#RearSecurityDoorCode").val(),
            "GarageDoor": $("#GarageDoor").val(),
            "Company": $("#DoorsCompany").val(),
            "CompanyPhone": $("#DoorsCompanyPhone").val(),
            "Windows": $("#Windows").val(),
            "Contractor": $("#DoorsContractor").val(),
            "ContractorPhone": $("#DoorsContractorPhone").val(),
            "StormWindows": $("#StormWindows").val(),
            "Other": $("#DoorsOther").val(),
        };
        data.electrical = {
            "ServiceCompany": $("#ServiceCompany").val(),
            "Phone": $("#ServiceCompanyPhone").val(),
            "AptCircuitBreaker": $("#AptCircuitBreaker").val(),
            "FuseBox": $("#FuseBox").val(),
            "LocationofService": $("#LocationofService").val(),
            "InApartment": $("#InApartment").val(),
            "Amps": $("#Amps").val(),
            "PublicCircuitBreaker": $("#PublicCircuitBreaker").val(),
            "PublicFuseBox": $("#PublicFuseBox").val(),
            "LightTimer": $("#LightTimer").val(),
            "PhotoCells": $("#PhotoCells").val(),
            "Location": $("#ElectricalLocation").val()
        };
        data.elevator = {
            "Contractor": $("#ElevatorContractor").val(),
            "ContractorPhone": $("#ElevatorContractorPhone").val(),
            "Passenger": $("#Passenger").val(),
            "numOfElevators": $("#numOfElevators").val(),
            "Freight": $("#Freight").val(),
            "ElevatorRoomLocation": $("#ElevatorRoomLocation").val(),
            "KeyType": $("#ElevatorKeyType").val(),
            "CommentsRepair": $("#CommentsRepair").val(),
            "InspectionDate": $("#InspectionDate").val(),
            "customerNumber": $("#customerNumber").val(),
            "ContactAndPhone": $("#ContactAndPhone").val(),
        };
        data.extermination = {
            "Company": $("#ExterminationCompany").val(),
            "CompanyPhone": $("#ExterminationCompanyPhone").val(),
            "Contact": $("#ExterminationContact").val(),
            "ContactPhone": $("#ExterminationContactPhone").val(),
            "Contract": $("#ExterminationContract").val(),
            "Frequency": $("#ExterminationFrequency").val()
        };
        data.firealarm = {
            "MonitorCompany": $("#MonitorCompany").val(),
            "MonitorCompanyPhone": $("#MonitorCompanyPhone").val(),
            "accountnumber": $("#accountnumber").val(),
            "TenantAlarm": $("#TenantAlarm").val(),
            "TenantAlarmPhone": $("#TenantAlarmPhone").val(),
            "EmergencyServiceCompany": $("#EmergencyServiceCompany").val(),
            "EmergencyServiceCompanyPhone": $("#EmergencyServiceCompanyPhone").val(),
            "Contract": $("#FireAlarmContract").val(),
            "Location": $("#FireAlarmLocation").val(),
            "Passcode": $("#FireAlarmPasscode").val(),
            "Central_Station": $("#Central_Station").val(),
            "PhonelinetoFireDept": $("#PhonelinetoFireDept").val(),
            "SmokeDetectors": $("#SmokeDetectors").val(),
            "HardWire": $("#HardWire").val(),
            "Battery": $("#Battery").val(),
            "HeatDetectors": $("#HeatDetectors").val(),
            "Located": $("#Located").val(),
            "AdditonalEquipment": $("#AdditonalEquipment").val(),
            "InspectionDate": $("#FireInspectionDate").val(),
            "KeyType": $("#KeyType").val(),
            "Code": $("#FireCode").val()
        };
        data.gas = {
            "Company": $("#GasCompany").val(),
            "SuppliedByLandlord": $("#SuppliedByLandlord").val(),
            "MeterLocation": $("#MeterLocation").val(),
            "Boilers": $("#Boilers").val(),
            "Stoves": $("#Stoves").val(),
            "EmergencyService": $("#GasEmergencyService").val(),
            "EmergencyPhone": $("#GasEmergencyPhone").val()
        };
        data.heating = {
            "type": $("#Type").val(),
            "location": $("#HeatSourceLocation").val(),
            "heatingCompany": $("#heatingCompany").val(),
            "phone": $("#HeatingCompanyPhone").val(),
            "other": $("#HeatingOther").val(),
            "contract": $("#HeatingContract").val(),
            "fhw": $("#fhw").val(),
            "steam": $("#steam").val(),
            "fha": $("#fha").val(),
            "electric": $("#electric").val(),
            "hvac": $("#hvac").val(),
            "basementappt": $("#basementappt").val(),
            "laundryroom": $("#laundryroom").val(),
            "laundrynotes": $("#laundrynotes").val(),
            "heatpaidforby": $("#heatpaidforby").val(),
            "electricpaidforby": $("#electricpaidForby").val(),
            "thcowned": $("#heatthcowned").val(),
            "individual": $("#individual").val(),
            "oilfilllocationsandtanksize": $("#oilfilllocationsandtanksize").val(),
            "hotwatersuppliedby": $("#hotwatersuppliedby").val(),
            "heattimerlocation": $("#heattimerlocation").val(),
            "boilercleanoutdate": $("#boilercleanoutdate").val(),
            "comments": $("#heatcomments").val()
        };
        data.laundry = {
            "Location": $("#LaundryLocation").val(),
            "ServiceCompany": $("#LaundryServiceCompany").val(),
            "ServiceCompanyPhone": $("#LaundryServiceCompanyPhone").val(),
            "THCowned": $("#THCowned").val(),
            "Leased": $("#Leased").val(),
            "NumberofWashers": $("#NumberofWashers").val(),
            "NumberofDryers": $("#NumberofDryers").val(),
            "CostofWashers": $("#CostofWashers").val(),
            "CostofDryers": $("#CostofDryers").val(),
            "MeterBoxCheckDate": $("#MeterBoxCheckDate").val(),
            "CardCoin": $("#CardCoin").val(),
            "VTMLocation": $("#VTMLocation").val(),
            "VTMLine": $("#VTMLine").val(),
            "CardCompany": $("#CardCompany").val()
        };
        data.locksmith = {
            "EmergencyLockSmith": $("#EmergencyLockSmith").val(),
            "EmergencyLockPhone": $("#EmergencyLockPhone").val(),
            "LocationOfKeys": $("#LocationOfKeys").val(),
            "FrontRearDoorMastered": $("#FrontRearDoorMastered").val(),
            "UnitsMastered": $("#UnitsMastered").val(),
            "BoilerRoomLockAndLocation": $("#BoilerRoomLockAndLocation").val(),
            "FobRepairContact": $("#FobRepairContact").val(),
            "FobOrdering": $("#FobOrdering").val()
        };
        data.personnel = {
            "24hroncallname": $("#24hroncallname").val(),
            "24hroncallphone": $("#24hroncallphone").val(),
            "24hroncallemail": $("#24hroncallemail").val(),
            "supername": $("#supername").val(),
            "superphone": $("#superphone").val(),
            "superemail": $("#superemail").val(),
            "superaddress": $("#superaddress").val(),
            "onsitemanagername": $("#onsitemanagername").val(),
            "onsitemanagerphone": $("#onsitemanagerphone").val(),
            "onsitemanageraddress": $("#onsitemanageraddress").val(),
            "securitydesk": $("#securitydesk").val(),
            "securitydeskphone": $("#securitydeskphone").val(),
            "afterhoursaccess": $("#afterhoursaccess").val(),
            "guardservice": $("#guardservice").val(),
            "guardservicephone": $("#guardservicephone").val(),
            "cleaningcompany": $("#cleaningcompany").val(),
            "cleaningcompanyphone": $("#cleaningcompanyphone").val(),
            "cleaningspecifications": $("#cleaningspecifications").val()
        };
        data.plumbing = {
            "EmergencyService": $("#plumbingEmergencyService").val(),
            "EmergencyPhone": $("#plumbingEmergencyServicePhone").val(),
            "BackUpService": $("#BackUpService").val(),
            "BackUpPhone": $("#BackUpPhone").val(),
            "EmergencySewerAndDrain": $("#EmergencySewerAndDrain").val(),
            "EmergencySewerAndDrainPhone": $("#EmergencySewerAndDrainPhone").val(),
            "FloodsWetVacCompany": $("#FloodsWetVacCompany").val(),
            "FloodsWetVacCompanyPhone": $("#FloodsWetVacCompanyPhone").val()
        };
        data.roof = {
            "Contractor": $("#RoofContractor").val(),
            "ContractorPhone": $("#RoofContractorPhone").val(),
            "RoofAccessLocation": $("#RoofAccessLocation").val(),
            "Type": $("#RoofType").val(),
            "Age": $("#RoofAge").val(),
            "Warranty": $("#RoofWarranty").val()
        };
        data.snow = {
            "PlowingTHCOrPrivate": $("#PlowingTHCOrPrivate").val(),
            "Contact": $("#PlowingContact").val(),
            "ContactPhone": $("#PlowingContactPhone").val(),
            "ShovelingTHCOrPrivate": $("#ShovelingTHCOrPrivate").val(),
            "ShovelingPhone": $("#ShovelingPhone").val(),
            "ShovelingContact": $("#ShovelingContact").val(),
            "ShovelingContactPhone": $("#ShovelingContactPhone").val(),
            "RemovalCompany": $("#RemovalCompany").val(),
            "RemovalPhone": $("#RemovalPhone").val(),
            "MinimumMatToPlow": $("#MinimumMatToPlow").val(),
            "MinimumToShovel": $("#MinimumToShovel").val()
        };
        data.sprinkler = {
            "Company": $("#SprinklerCompany").val(),
            "Phone": $("#SprinklerCompanyPhone").val(),
            "Contract": $("#SprinklerContract").val(),
            "EmergencyPhone": $("#SprinklerEmergencyPhone").val(),
            "ContactPerson": $("#ContactPerson").val(),
            "CommonHallways": $("#CommonHallways").val(),
            "InsideUnits": $("#InsideUnits").val(),
            "WetOrDry": $("#WetOrDry").val(),
            "InspectionDate": $("#SprinklerInspectionDate").val(),
            "LocationOfShutOff": $("#LocationOfShutOff").val()
        };
        data.trash = {
            "Dumpsters": $("#Dumpsters").val(),
            "OwnedOrLeased": $("#OwnedOrLeased").val(),
            "Type": $("#DumpsterType").val(),
            "Location": $("#DumpsterLocation").val(),
            "PickUpTrashDays": $("#PickUpTrashDays").val(),
            "CityPickUp": $("#CityPickUp").val(),
            "CityTrashPhone": $("#CityTrashPhone").val(),
            "PrivatePickUp": $("#PrivatePickUp").val(),
            "Other": $("#TrashOther").val()
        };
        data.water = {
            "TotalNumOfMeters": $("#TotalNumOfMeters").val(),
            "KeyType": $("#WaterKeyType").val(),
            "Code": $("#WaterCode").val(),
            "MainShutOffLocation": $("#MainShutOffLocation").val(),
            "MeterReadDate": $("#MeterReadDate").val()
        };
        data.towing = {
            "TowingCompany": $("#TowingCompany").val(),
            "TowingPhone": $("#TowingPhone").val()
        };
        $.post("/new", data, function() {
            window.location.href="/";
        }, "json");
    });
});