var m = {};
var Hashes = require("jshashes");
m.connect = function(cb) {
    var mysql = require('mysql');
    var connection = mysql.createConnection({
        host: '108.7.54.53',
        user: 'root',
        password: 'Hamilton',
        database: 'hamilton'
    });

    connection.connect(function(err) {
        if (err) {
            console.error('error connecting: ' + err.stack);
            return;
        }
        console.log('connected as id ' + connection.threadId);
    });
    cb(connection);
};
m.clean = function(obj) {
    for (var propName in obj) {

        for (var propp in obj[propName]) {
            if (obj[propName][propp] === null || obj[propName][propp] === undefined) {
                delete obj[propName][propp];
            }
        }

        if (obj[propName] === null || obj[propName] === undefined) {
            delete obj[propName];
        }
    }
};
m.queryProperty = function(connection, property, cb) {
    var prop = {};
    connection.query('SELECT * FROM tbl_properties WHERE Property=' + connection.escape(property), function(error, results, fields) {
        if (error) throw error;
        prop = results[0];
    });

    connection.query('SELECT * FROM tbl_roof WHERE Property=' + connection.escape(property), function(error, results, fields) {
        if (error) throw error;
        if (!prop) prop = {};
        prop["Roof"] = results[0];
    });
    connection.query('SELECT * FROM tbl_doors_and_windows WHERE Property=' + connection.escape(property), function(error, results, fields) {
        if (error) throw error;
        prop["Doors and Windows"] = results[0];
    });
    connection.query('SELECT * FROM tbl_electrical WHERE Property=' + connection.escape(property), function(error, results, fields) {
        if (error) throw error;
        prop["Electrical"] = results[0];
    });
    connection.query('SELECT * FROM tbl_elevator WHERE Property=' + connection.escape(property), function(error, results, fields) {
        if (error) throw error;
        prop["Elevator"] = results[0];
    });
    connection.query('SELECT * FROM tbl_extermination WHERE Property=' + connection.escape(property), function(error, results, fields) {
        if (error) throw error;
        prop["Extermination"] = results[0];
    });
    connection.query('SELECT * FROM tbl_fire_alarm WHERE Property=' + connection.escape(property), function(error, results, fields) {
        if (error) throw error;
        prop["Fire Alarm"] = results[0];
    });
    connection.query('SELECT * FROM tbl_gas WHERE Property=' + connection.escape(property), function(error, results, fields) {
        if (error) throw error;
        prop["Gas"] = results[0];
    });
    connection.query('SELECT * FROM tbl_heating WHERE Property=' + connection.escape(property), function(error, results, fields) {
        if (error) throw error;
        prop["Heating"] = results[0];
    });
    connection.query('SELECT * FROM tbl_laundry WHERE Property=' + connection.escape(property), function(error, results, fields) {
        if (error) throw error;
        prop["Laundry"] = results[0];
    });
    connection.query('SELECT * FROM tbl_locksmith WHERE Property=' + connection.escape(property), function(error, results, fields) {
        if (error) throw error;
        prop["Locksmith"] = results[0];
    });
    connection.query('SELECT * FROM tbl_personnel_cleaning WHERE Property=' + connection.escape(property), function(error, results, fields) {
        if (error) throw error;
        prop["Personnel and Cleaning"] = results[0];
    });
    connection.query('SELECT * FROM tbl_plumbing WHERE Property=' + connection.escape(property), function(error, results, fields) {
        if (error) throw error;
        prop["Plumbing"] = results[0];
    });
    connection.query('SELECT * FROM tbl_snow_services WHERE Property=' + connection.escape(property), function(error, results, fields) {
        if (error) throw error;
        prop["Snow Services"] = results[0];
    });
    connection.query('SELECT * FROM tbl_sprinkler_system WHERE Property=' + connection.escape(property), function(error, results, fields) {
        if (error) throw error;
        prop["Sprinkler System"] = results[0];
    });
    connection.query('SELECT * FROM tbl_towing WHERE Property=' + connection.escape(property), function(error, results, fields) {
        if (error) throw error;
        prop["Towing"] = results[0];
    });
    connection.query('SELECT * FROM tbl_trash WHERE Property=' + connection.escape(property), function(error, results, fields) {
        if (error) throw error;
        prop["Trash"] = results[0];
    });
    connection.query('SELECT * FROM tbl_water_meter WHERE Property=' + connection.escape(property), function(error, results, fields) {
        if (error) throw error;
        prop["Water Meter"] = results[0];
    });
    /*connection.query('SELECT * FROM tbl_misc WHERE Property=' + connection.escape(property), function(error, results, fields) {
        if (error) throw error;
        prop.misc = results[0];
    });
    */
    connection.query('SELECT * FROM tbl_directions WHERE Property=' + connection.escape(property), function(error, results, fields) {
        if (error) throw error;
        prop["Directions"] = results[0];
    });
    connection.query('SELECT * FROM tbl_leasing WHERE Property=' + connection.escape(property), function(error, results, fields) {
        if (error) throw error;
        prop["Leasing"] = results[0];
    });
    connection.query('SELECT * FROM tbl_ac WHERE Property=' + connection.escape(property), function(error, results, fields) {
        if (error) throw error;
        prop["AC"] = results[0];
        m.clean(prop);
        cb(prop);
    });

};
m.getAllProperties = function(connection, cb) {
    connection.query("SELECT property, Address FROM tbl_properties", function(error, results, fields) {
        cb(results);
    });
};
m.createNewProperty = function(connection, data, cb) {
    m.createNewProperties(connection, data);
    m.createNewAC(connection, data);
    m.createNewDoors(connection, data);
    m.createNewElectrical(connection, data);
    m.createNewElevator(connection, data);
    m.createNewExtermination(connection, data);
    m.createNewFireAlarm(connection, data);
    m.createNewGas(connection, data);
    m.createNewHeating(connection, data);
    m.createNewLaundry(connection, data);
    m.createNewLocksmith(connection, data);
    m.createNewPersonnel(connection, data);
    m.createNewPlumbing(connection, data);
    m.createNewRoof(connection, data);
    m.createNewSnow(connection, data);
    m.createNewSprinkler(connection, data);
    m.createNewTowing(connection, data);
    m.createNewTrash(connection, data);
    m.createNewWater(connection, data);
};
m.createNewProperties = function(connection, data) {
    var sql = 'INSERT INTO tbl_properties(Property,';
    var sqlmiddle = ") VALUES('" + data.propertyid + "',";
    var names = [];
    var da = [];
    for (var type in data.prop) {
        names.push(type);
        da.push(data.prop[type]);
    }
    for (var x in names) {
        if (x === '0') sql += names[x];
        else {
            sql += "," + names[x];
        }
    }
    sql += sqlmiddle;
    for (var y in da) {
        if (y === '0') sql += connection.escape(da[y]);
        else {
            sql += "," + connection.escape(da[y]);
        }
    }
    sql += ");";
    connection.query(sql);

};
m.createNewAC = function(connection, data) {
    var sql = 'INSERT INTO tbl_ac(Property,';
    var sqlmiddle = ") VALUES('" + data.propertyid + "',";
    var names = [];
    var da = [];
    for (var type in data.ac) {
        names.push(type);
        da.push(data.ac[type]);
    }
    for (var x in names) {
        if (x === '0') sql += names[x];
        else {
            sql += "," + names[x];
        }
    }
    sql += sqlmiddle;
    for (var y in da) {
        if (y === '0') sql += connection.escape(da[y]);
        else {
            sql += "," + connection.escape(da[y]);
        }
    }
    sql += ");";
    connection.query(sql);

};
m.createNewDoors = function(connection, data) {
    var sql = 'INSERT INTO tbl_doors_and_windows(Property,';
    var sqlmiddle = ") VALUES('" + data.propertyid + "',";
    var names = [];
    var da = [];
    for (var type in data.doors) {
        names.push(type);
        da.push(data.doors[type]);
    }
    for (var x in names) {
        if (x === '0') sql += names[x];
        else {
            sql += "," + names[x];
        }
    }
    sql += sqlmiddle;
    for (var y in da) {
        if (y === '0') sql += connection.escape(da[y]);
        else {
            sql += "," + connection.escape(da[y]);
        }
    }
    sql += ");";
    connection.query(sql);

};
m.createNewElectrical = function(connection, data) {
    var sql = 'INSERT INTO tbl_electrical(Property,';
    var sqlmiddle = ") VALUES('" + data.propertyid + "',";
    var names = [];
    var da = [];
    for (var type in data.electrical) {
        names.push(type);
        da.push(data.electrical[type]);
    }
    for (var x in names) {
        if (x === '0') sql += names[x];
        else {
            sql += "," + names[x];
        }
    }
    sql += sqlmiddle;
    for (var y in da) {
        if (y === '0') sql += connection.escape(da[y]);
        else {
            sql += "," + connection.escape(da[y]);
        }
    }
    sql += ");";
    connection.query(sql);

};
m.createNewElevator = function(connection, data) {
    var sql = 'INSERT INTO tbl_elevator(Property,';
    var sqlmiddle = ") VALUES('" + data.propertyid + "',";
    var names = [];
    var da = [];
    for (var type in data.elevator) {
        names.push(type);
        da.push(data.elevator[type]);
    }
    for (var x in names) {
        if (x === '0') sql += names[x];
        else {
            sql += "," + names[x];
        }
    }
    sql += sqlmiddle;
    for (var y in da) {
        if (y === '0') sql += connection.escape(da[y]);
        else {
            sql += "," + connection.escape(da[y]);
        }
    }
    sql += ");";
    connection.query(sql);

};
m.createNewExtermination = function(connection, data) {
    var sql = 'INSERT INTO tbl_extermination(Property,';
    var sqlmiddle = ") VALUES('" + data.propertyid + "',";
    var names = [];
    var da = [];
    for (var type in data.extermination) {
        names.push(type);
        da.push(data.extermination[type]);
    }
    for (var x in names) {
        if (x === '0') sql += names[x];
        else {
            sql += "," + names[x];
        }
    }
    sql += sqlmiddle;
    for (var y in da) {
        if (y === '0') sql += connection.escape(da[y]);
        else {
            sql += "," + connection.escape(da[y]);
        }
    }
    sql += ");";
    connection.query(sql);

};
m.createNewFireAlarm = function(connection, data) {
    var sql = 'INSERT INTO tbl_fire_alarm(Property,';
    var sqlmiddle = ") VALUES('" + data.propertyid + "',";
    var names = [];
    var da = [];
    for (var type in data.firealarm) {
        names.push(type);
        da.push(data.firealarm[type]);
    }
    for (var x in names) {
        if (x === '0') sql += names[x];
        else {
            sql += "," + names[x];
        }
    }
    sql += sqlmiddle;
    for (var y in da) {
        if (y === '0') sql += connection.escape(da[y]);
        else {
            sql += "," + connection.escape(da[y]);
        }
    }
    sql += ");";
    connection.query(sql);

};
m.createNewGas = function(connection, data) {
    var sql = 'INSERT INTO tbl_gas(Property,';
    var sqlmiddle = ") VALUES('" + data.propertyid + "',";
    var names = [];
    var da = [];
    for (var type in data.gas) {
        names.push(type);
        da.push(data.gas[type]);
    }
    for (var x in names) {
        if (x === '0') sql += names[x];
        else {
            sql += "," + names[x];
        }
    }
    sql += sqlmiddle;
    for (var y in da) {
        if (y === '0') sql += connection.escape(da[y]);
        else {
            sql += "," + connection.escape(da[y]);
        }
    }
    sql += ");";
    connection.query(sql);

};
m.createNewHeating = function(connection, data) {
    var sql = 'INSERT INTO tbl_Heating(Property,';
    var sqlmiddle = ") VALUES('" + data.propertyid + "',";
    var names = [];
    var da = [];
    for (var type in data.heating) {
        names.push(type);
        da.push(data.heating[type]);
    }
    for (var x in names) {
        if (x === '0') sql += names[x];
        else {
            sql += "," + names[x];
        }
    }
    sql += sqlmiddle;
    for (var y in da) {
        if (y === '0') sql += connection.escape(da[y]);
        else {
            sql += "," + connection.escape(da[y]);
        }
    }
    sql += ");";
    connection.query(sql);

};
m.createNewLaundry = function(connection, data) {
    var sql = 'INSERT INTO tbl_laundry(Property,';
    var sqlmiddle = ") VALUES('" + data.propertyid + "',";
    var names = [];
    var da = [];
    for (var type in data.laundry) {
        names.push(type);
        da.push(data.laundry[type]);
    }
    for (var x in names) {
        if (x === '0') sql += names[x];
        else {
            sql += "," + names[x];
        }
    }
    sql += sqlmiddle;
    for (var y in da) {
        if (y === '0') sql += connection.escape(da[y]);
        else {
            sql += "," + connection.escape(da[y]);
        }
    }
    sql += ");";
    connection.query(sql);

};
m.createNewLocksmith = function(connection, data) {
    var sql = 'INSERT INTO tbl_locksmith(Property,';
    var sqlmiddle = ") VALUES('" + data.propertyid + "',";
    var names = [];
    var da = [];
    for (var type in data.locksmith) {
        names.push(type);
        da.push(data.locksmith[type]);
    }
    for (var x in names) {
        if (x === '0') sql += names[x];
        else {
            sql += "," + names[x];
        }
    }
    sql += sqlmiddle;
    for (var y in da) {
        if (y === '0') sql += connection.escape(da[y]);
        else {
            sql += "," + connection.escape(da[y]);
        }
    }
    sql += ");";
    connection.query(sql);

};
m.createNewPersonnel = function(connection, data) {
    var sql = 'INSERT INTO tbl_personnel_cleaning(Property,';
    var sqlmiddle = ") VALUES('" + data.propertyid + "',";
    var names = [];
    var da = [];
    for (var type in data.personnel) {
        names.push(type);
        da.push(data.personnel[type]);
    }
    for (var x in names) {
        if (x === '0') sql += names[x];
        else {
            sql += "," + names[x];
        }
    }
    sql += sqlmiddle;
    for (var y in da) {
        if (y === '0') sql += connection.escape(da[y]);
        else {
            sql += "," + connection.escape(da[y]);
        }
    }
    sql += ");";
    connection.query(sql);

};
m.createNewPlumbing = function(connection, data) {
    var sql = 'INSERT INTO tbl_plumbing(Property,';
    var sqlmiddle = ") VALUES('" + data.propertyid + "',";
    var names = [];
    var da = [];
    for (var type in data.plumbing) {
        names.push(type);
        da.push(data.plumbing[type]);
    }
    for (var x in names) {
        if (x === '0') sql += names[x];
        else {
            sql += "," + names[x];
        }
    }
    sql += sqlmiddle;
    for (var y in da) {
        if (y === '0') sql += connection.escape(da[y]);
        else {
            sql += "," + connection.escape(da[y]);
        }
    }
    sql += ");";
    connection.query(sql);

};
m.createNewRoof = function(connection, data) {
    var sql = 'INSERT INTO tbl_roof(Property,';
    var sqlmiddle = ") VALUES('" + data.propertyid + "',";
    var names = [];
    var da = [];
    for (var type in data.roof) {
        names.push(type);
        da.push(data.roof[type]);
    }
    for (var x in names) {
        if (x === '0') sql += names[x];
        else {
            sql += "," + names[x];
        }
    }
    sql += sqlmiddle;
    for (var y in da) {
        if (y === '0') sql += connection.escape(da[y]);
        else {
            sql += "," + connection.escape(da[y]);
        }
    }
    sql += ");";
    connection.query(sql);

};
m.createNewSnow = function(connection, data) {
    var sql = 'INSERT INTO tbl_snow_services(Property,';
    var sqlmiddle = ") VALUES('" + data.propertyid + "',";
    var names = [];
    var da = [];
    for (var type in data.snow) {
        names.push(type);
        da.push(data.snow[type]);
    }
    for (var x in names) {
        if (x === '0') sql += names[x];
        else {
            sql += "," + names[x];
        }
    }
    sql += sqlmiddle;
    for (var y in da) {
        if (y === '0') sql += connection.escape(da[y]);
        else {
            sql += "," + connection.escape(da[y]);
        }
    }
    sql += ");";
    connection.query(sql);

};
m.createNewSprinkler = function(connection, data) {
    var sql = 'INSERT INTO tbl_sprinkler_system(Property,';
    var sqlmiddle = ") VALUES('" + data.propertyid + "',";
    var names = [];
    var da = [];
    for (var type in data.sprinkler) {
        names.push(type);
        da.push(data.sprinkler[type]);
    }
    for (var x in names) {
        if (x === '0') sql += names[x];
        else {
            sql += "," + names[x];
        }
    }
    sql += sqlmiddle;
    for (var y in da) {
        if (y === '0') sql += connection.escape(da[y]);
        else {
            sql += "," + connection.escape(da[y]);
        }
    }
    sql += ");";
    connection.query(sql);

};
m.createNewTowing = function(connection, data) {
    var sql = 'INSERT INTO tbl_towing(Property,';
    var sqlmiddle = ") VALUES('" + data.propertyid + "',";
    var names = [];
    var da = [];
    for (var type in data.towing) {
        names.push(type);
        da.push(data.towing[type]);
    }
    for (var x in names) {
        if (x === '0') sql += names[x];
        else {
            sql += "," + names[x];
        }
    }
    sql += sqlmiddle;
    for (var y in da) {
        if (y === '0') sql += connection.escape(da[y]);
        else {
            sql += "," + connection.escape(da[y]);
        }
    }
    sql += ");";
    connection.query(sql);

};
m.createNewWater = function(connection, data) {
    var sql = 'INSERT INTO tbl_water_meter(Property,';
    var sqlmiddle = ") VALUES('" + data.propertyid + "',";
    var names = [];
    var da = [];
    for (var type in data.water) {
        names.push(type);
        da.push(data.water[type]);
    }
    for (var x in names) {
        if (x === '0') sql += names[x];
        else {
            sql += "," + names[x];
        }
    }
    sql += sqlmiddle;
    for (var y in da) {
        if (y === '0') sql += connection.escape(da[y]);
        else {
            sql += "," + connection.escape(da[y]);
        }
    }
    sql += ");";
    connection.query(sql);

};
m.createNewTrash = function(connection, data) {
    var sql = 'INSERT INTO tbl_trash(Property,';
    var sqlmiddle = ") VALUES('" + data.propertyid + "',";
    var names = [];
    var da = [];
    for (var type in data.trash) {
        names.push(type);
        da.push(data.trash[type]);
    }
    for (var x in names) {
        if (x === '0') sql += names[x];
        else {
            sql += "," + names[x];
        }
    }
    sql += sqlmiddle;
    for (var y in da) {
        if (y === '0') sql += connection.escape(da[y]);
        else {
            sql += "," + connection.escape(da[y]);
        }
    }
    sql += ");";
    connection.query(sql);

};
m.deleteProperty = function(connection, prop) {
    connection.query("DELETE FROM tbl_ac WHERE Property=" + connection.escape(prop), function(error) {
        if (error) throw error;
    });
    connection.query("DELETE FROM tbl_doors_and_windows WHERE Property=" + connection.escape(prop), function(error) {
        if (error) throw error;
    });
    connection.query("DELETE FROM tbl_electrical WHERE Property=" + connection.escape(prop), function(error) {
        if (error) throw error;
    });
    connection.query("DELETE FROM tbl_elevator WHERE Property=" + connection.escape(prop), function(error) {
        if (error) throw error;
    });
    connection.query("DELETE FROM tbl_extermination WHERE Property=" + connection.escape(prop), function(error) {
        if (error) throw error;
    });
    connection.query("DELETE FROM tbl_fire_alarm WHERE Property=" + connection.escape(prop), function(error) {
        if (error) throw error;
    });
    connection.query("DELETE FROM tbl_gas WHERE Property=" + connection.escape(prop), function(error) {
        if (error) throw error;
    });
    connection.query("DELETE FROM tbl_heating WHERE Property=" + connection.escape(prop), function(error) {
        if (error) throw error;
    });
    connection.query("DELETE FROM tbl_laundry WHERE Property=" + connection.escape(prop), function(error) {
        if (error) throw error;
    });
    connection.query("DELETE FROM tbl_leasing WHERE Property=" + connection.escape(prop), function(error) {
        if (error) throw error;
    });
    connection.query("DELETE FROM tbl_locksmith WHERE Property=" + connection.escape(prop), function(error) {
        if (error) throw error;
    });
    connection.query("DELETE FROM tbl_personnel_cleaning WHERE Property=" + connection.escape(prop), function(error) {
        if (error) throw error;
    });
    connection.query("DELETE FROM tbl_plumbing WHERE Property=" + connection.escape(prop), function(error) {
        if (error) throw error;
    });
    connection.query("DELETE FROM tbl_properties WHERE Property=" + connection.escape(prop), function(error) {
        if (error) throw error;
    });
    connection.query("DELETE FROM tbl_roof WHERE Property=" + connection.escape(prop), function(error) {
        if (error) throw error;
    });
    connection.query("DELETE FROM tbl_snow_services WHERE Property=" + connection.escape(prop), function(error) {
        if (error) throw error;
    });
    connection.query("DELETE FROM tbl_sprinkler_system WHERE Property=" + connection.escape(prop), function(error) {
        if (error) throw error;
    });
    connection.query("DELETE FROM tbl_towing WHERE Property=" + connection.escape(prop), function(error) {
        if (error) throw error;
    });
    connection.query("DELETE FROM tbl_trash WHERE Property=" + connection.escape(prop), function(error) {
        if (error) throw error;
    });
    connection.query("DELETE FROM tbl_water_meter WHERE Property=" + connection.escape(prop), function(error) {
        if (error) throw error;
    });
};
m.editProperty = function(connection, data) {
    m.deleteProperty(connection, data.propertyid);
    m.createNewProperty(connection, data);
};
m.addUser = function(connection, user, pass, perms,cb) {
    var hash = new Hashes.SHA1().b64(pass);
    connection.query("INSERT INTO tbl_users(name,password,permissions) VALUES('" + user + "','" + hash + "','" + perms + "');", function(error) {
        if(error) cb(false);
        else cb(true);
    });
};
m.checkUserPass = function(connection, user, passgiven, cb) {
    connection.query("SELECT * FROM tbl_users WHERE name=" + connection.escape(user) + ";", function(error, results, fields) {
        if (error) throw error;
        if (!results[0]) {
            cb("Username not found");
        } else {
            var newhash = new Hashes.SHA1().b64(passgiven);
            if (results[0].password === newhash) {
                cb(true);
            } else {
                cb("Wrong password!");
            }
        }
    });
};
m.getPerms = function(connection,user,cb){
  connection.query("SELECT permissions FROM tbl_users WHERE name="+connection.escape(user)+";",function(error,results,fields){
      if(results[0]) cb(results[0].permissions);
      else{
          cb("n");
      }
  });  
};
module.exports = m;