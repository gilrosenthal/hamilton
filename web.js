var express = require('express'),
    api = require("./api"),
    fs = require("fs"),
    bodyParser = require('body-parser'),
    app = express(),
    connection,
    dictionary = JSON.parse(fs.readFileSync(__dirname + "/dictionary.json"));
    
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

api.connect(function(conn) {
    connection = conn;
});
app.use(express.static('static'));
app.get("/api/list", function(req, res) {
    api.getAllProperties(connection, function(results) {
        res.json(results);
    });
});
app.get("/new", function(req, res) {
    res.sendFile(__dirname + "/static/new.html");
});
app.get("/signup", function(req, res) {
    res.sendFile(__dirname + "/static/register.html");
});
app.get("/register", function(req, res) {
    res.sendFile(__dirname + "/static/register.html");
});
app.post("/new", function(req, res) {
    api.createNewProperty(connection, req.body);
    res.redirect("/property/" + req.body.propertyid);
});
app.get("/", function(req, res) {
    res.sendFile(__dirname + "/static/index.html");
});
app.get("/property/:prop", function(req, res) {
    res.cookie("propname", req.params.prop.replace("api", ""));
    res.sendFile(__dirname + "/static/basepage.html");
});
app.get("/property/:prop/edit", function(req, res) {
    res.cookie("propname", req.params.prop);
    res.sendFile(__dirname + "/static/baseedit.html");
});
app.post("/property/:prop/edit", function(req, res) {
    api.editProperty(connection, req.body);
    res.redirect("/");
});
app.get('/api/:id', function(req, res) {
    if (req.params.id !== "favicon.ico") {
        api.queryProperty(connection, req.params.id, function(results) {
            if (results.toString() !== "{}") {
                var result = {};
                for (var prop in results) {
                    result[prop] = {
                        "desc": dictionary[prop],
                        "value": results[prop]
                    };
                }
                for (var element in result) {
                    for (var i in result[element].desc) {
                        if (!result[element].value[i]) {
                            delete result[element].value[i];
                        } else {
                            if (result[element].desc[i].indexOf("Yes") !== -1) {
                                if (result[element].value[i].toString() === "0") result[element].value[i] = "No";
                                else if (result[element].value[i].toString() === "1") result[element].value[i] = "Yes";
                            }
                        }
                    }
                }
                for (var x in result) {
                    var count = Object.keys(result[x].value).length;
                    if (count === 1 && Object.keys(result[x].desc).length > 1) {
                        delete(result[x]);
                    }
                }
                if (result.Property) {
                    res.json(result);
                } else {
                    res.json({
                        'Alert': "Property not found!"
                    });
                }
            }
        });
    }

});
app.post("/signup", function(req, res) {
    console.log(req.body);
    api.addUser(connection, req.body.user, req.body.password, req.body.roles, function(r) {
        if (r) {
            res.cookie("sessionid", new Buffer(req.body.user).toString('base64'), {
                encode: String
            });
            res.redirect("/");
        } else {
            res.cookie("sessionid", "", {
                encode: String
            });
            res.cookie("error", "Username already taken!", {
                encode: String
            });
            res.redirect("/");
        }
    });

});
app.get("/login", function(req, res) {
    res.sendFile(__dirname + "/static/login.html");
});
app.post("/login", function(req, res) {
    api.checkUserPass(connection,req.body.user,req.body.password,function(r){
        if(r===true){
             res.cookie("sessionid", new Buffer(req.body.user).toString('base64'), {
                encode: String
            });
            res.redirect("/");
        }
        else{
            res.cookie("error",r,{
                encode: String
            });
            res.redirect("/");
        }
    });
});
app.get("/perms/:user",function(req,res){
    api.getPerms(connection,req.params.user,function(perms){
        res.send(perms);
    });
});
app.listen(8080);